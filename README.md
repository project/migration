# Migration Ğ1 v2

## Table des matières

- [Objectif](projects/index.md#objectif)
- [Comment ?](projects/index.md#comment-)
- [Travail en commissions](projects/index.md#travail-en-commissions)
- [Liste des commissions](projects/index.md#liste-des-commissions)
- [Compte-rendus de réunions](projects/index.md#compte-rendus-de-réunions)
- [Visio du lundi](projects/index.md#visio-du-lundi)

## Licence

Documents et code source sous [Licence CC-BY-SA](./LICENSE).

## Liens

 - Site web : https://doc.cesium.app/migration (adresse web temporaire)
