# Communication

L'objectif est **de communiquer** sur la **migration v2**, auprès de la communauté.

Profiter de l'occasion pour **toucher le grand public**.

---
### Liens


- Forum : [Migration Ğ1 v2](https://forum.monnaie-libre.fr/t/a-propos-de-la-categorie-migration-v2/29788) (monnaie-libre.fr)
- Télégram : [Migration Ğ1 v2](https://t.me/monnaielibrejune/57035) (groupe `@monnaielibrejune`)

---
## Documents de travail

> En cours d'élaboration (non diffusable)

- [Qu'est-ce que la V2 des logiciels de la Ğ1 ?](https://pad.p2p.legal/LogicielsV2pour%C4%9E1#)
- [Questions posées par la communauté Ğ1 - Mars 2024](https://pad.p2p.legal/s/questions-DV2s#)


---
## Actions

- Maaltir coordonne la relecture avec les développeurs
- Proposition de Benoit de faire une démo Cesium² auprès d'utilisateurs référents (avancés)
