# Développement

L'objectif est de **développer** les logiciels de la v2 et d'arriver à un état iso-fonctionnel permettant de migrer la Ğ1 sur l'écosystème v2 sans perte de fonctionnalité notable pour les utilisateurices.

---

## Avancement / Duniter v2 & GCli

Duniter est la partie blockchain qui intègre toutes les fonctionnalités soumises au consensus global. Ğcli est un utilitaire qui facilite grandement la gestion de l'identité et du statut forgeron par rapport à polkadotjs et en attendant l'implémentation complète dans les clients comme Cesium et Tikka.

| Logiciel   | Tâches                                | État          | Date | Priorité    |
| ---------- | ------------------------------------- | ------------- | ---- | ----------- |
| Duniter v2 |                                       |               |      |             |
|            | Commentaires de TX                    | Proto réalisé |      | v2 (client) |
|            | Précalcul de règle de distance        | WS à faire    |      | v2 (client) |
|            | Découverte des endpoint RPC           | à faire       |      | v2.1        |
|            | Anonymisation des TX (Ring signature) | Dev           |      | v3          |
| Ğcli       |                                       |               |      |             |
|            | Tout est fait                         | à lisser      |      |             |

---

## Avancement / Indexer & Data pod v2

L'indexeur duniter-squid permet d'indexer les données blockchain et de les exposer via une API GraphQL simple à prendre en main dans les clients.
Les datapods permettent de stocker et accéder à de la donnée hors chaîne via IPFS, et d'indexer ces données hors chaîne pour les rendre disponibles via une API GraphQL (ou elasticsearch) permettant la recherche sur serveur sans récupérer toute la donnée localement.

| Logiciel | Tâches                       | État    |
| -------- | ---------------------------- | ------- |
| Indexer  |                              |         |
|          | Commentaire de TX            | Réalisé |
|          | Historique des DU            | Réalisé |
|          | Offences                     | à faire |
|          | Historique adhésion forgeron | à faire |
|          | Frais de tx                  | à faire |
| Data pod |                              |         |
|          | Oplog onchain                | à faire |

---

## Avancement / Cesium²

Cesium² est la suite logique de Cesium. Elle vise à être iso-fonctionnelle et ne pas perdre les utilisateurices de la Ğ1. C'est également le client principal disponible sur le plus de plateformes ce qui lui donne une position prépondérante dans l'accueil de nouveaux utilisateurices de la Ğ1.

| Logiciel | Tâches                                                  | Notes             |
| -------- | ------------------------------------------------------- | ----------------- |
| Cesium²  |                                                         |                   |
|          | Parcours d'identité (création, certification, adhésion) | (version `alpha`) |
|          | Passage en API Relay Hasura.                            | Réalisé           |
|          | Amélioration performances                               | (version `alpha`) |
|          | Méthode d'authentification par défaut                   | (version `alpha`) |
|          | Affichage du DU                                         | (version `alpha`) |
|          | Renouvellement de certification                         | (version `alpha`) |
|          | Renouvellement d'adhésion                               | (version `alpha`) |

---

## Avancement / Autres clients

| Logiciel | Avancement       | Notes |
| -------- | ---------------- | ----- |
| Gecko    | ?                |       |
| G1nkgo   | V2 non débutée ? |       |
| Autre ?  |                  |       |

---

## Rétro-planning

> Exemple fictif (à compléter)

![Gantt](projects/dev/gantt.svg)
