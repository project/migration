
## Répartition en commissions

Visio du lundi **13h15 - 14h**
 
---

À **quelle commission** voulez-vous participer ? 

<table>
<tr>
    <th><i class="fa fa-plus-circle"></i> Le plus ?</th>
    <th><i class="fa fa-minus-circle"></i> Le moins ?</th>
</tr>
<tr>
    <td>
        <div data-poll="projects-plus" class="poll">
            <button data-value="dev">Développement</button>
            <button data-value="com">Communication</button>
            <button data-value="users">Utilisateurs</button>
            <button data-value="smiths">Forgerons</button>
            <button data-value="lang">Traduction</button>
        </div>
    </td>
    <td>
        <div data-poll="projects-minus" class="poll">
            <button data-value="dev">Développement</button>
            <button data-value="com">Communication</button>
            <button data-value="users">Utilisateurs</button>
            <button data-value="smiths">Forgerons</button>
            <button data-value="lang">Traduction</button>
        </div>
    </td>
</tr>
</table>

<button onclick="App.showQrCode()">
Générer un QR Code
</button>


---


<table>
<tr>
<th><i class="fa fa-plus-circle"></i> Le plus ?</th>
<th><i class="fa fa-minus-circle"></i> Le moins ?</th>
</tr>
<tr>
<td> 
<div style="height:300px; max-width: 450px">
	<canvas data-chart="bar" data-poll="projects-plus" style="min-width: 250px">
        <!--
        {
"options": { "elements": {"line": { "borderWidth": 3 } } },
         "data": {
            "labels": ["Coordination","Développement","Communication","Utilisateurs","Forgerons","Traduction"],
            "datasets": [
             {
                "label": "Les plus",
                "data":[0,0,0,0,0],
                "backgroundColor": "rgba(220,20,220,.7)"
             }
            ]
         }
        }
        -->
    </canvas>
</div>
</td>
<td> 
<div style="height:300px; max-width: 450px">
	<canvas data-chart="bar" data-poll="projects-minus" style="min-width: 250px">
        <!--
        {
         "data": {
            "labels": ["Coordination","Développement","Communication","Utilisateurs","Forgerons","Traduction"],
            "datasets":[
             {
                "label": "Les moins",
                "data":[0,0,0,0,0],
                "backgroundColor": "rgba(200,50,50,.7)"
             }
            ]
         }
        }
        -->
    </canvas>
</div>
</td>
</tr>
</table>

---

Commissions de **13h15 - 14h** :

|                                                           |                                                                                                       |
|-----------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| <i class="fa fa-home"></i> Accueil                        | [meet.evolix.org/g1v2-accueil](https://meet.evolix.org/g1v2-accueil)<!-- .element target="_blank" --> |
| <i class="fa fa-calendar"></i> Coordination               | [meet.evolix.org/g1v2-coord](https://meet.evolix.org/g1v2-coord)<!-- .element target="_blank" -->     |
| <i class="fa fa-bug"></i> Développement                   | [meet.evolix.org/g1v2-dev](https://meet.evolix.org/g1v2-dev)<!-- .element target="_blank" -->         |
| <i class="fa fa-bullhorn"></i> Communication              | [meet.evolix.org/g1v2-com](https://meet.evolix.org/g1v2-com)<!-- .element target="_blank" -->         |
| <i class="fa fa-users"></i> Accompagnement / Utilisateurs | [meet.evolix.org/g1v2-users](https://meet.evolix.org/g1v2-users)<!-- .element target="_blank" -->     |
| <i class="fa fa-cloud"></i> Accompagnement / Forgerons    | [meet.evolix.org/g1v2-smiths](https://meet.evolix.org/g1v2-smiths)<!-- .element target="_blank" -->   |
| <i class="fa fa-flag"></i> Traduction                     | [meet.evolix.org/g1v2-lang](https://meet.evolix.org/g1v2-lang)<!-- .element target="_blank" -->       |

**14h retour** en plénière : [meet.evolix.org/coordinationV2](https://meet.evolix.org/coordinationV2)<!-- .element target="_blank" -->
