
<!-- .slide: data-visibility="hidden" -->
## Sommaire

- [Objectif](#objectif)
- [Comment ?](#comment-)
- [Travail en commissions](#travail-en-commissions)
- [Liste des commissions](#liste-des-commissions)
- [Compte-rendus de réunions](#compte-rendus-de-réunions)
- [Visio du lundi](#visio-du-lundi)

---
## Objectif

L'objectif, pour continuer de **développer** la Ǧ1 (June), 

et de **migrer** la Ǧ1 en **version 2**

**sans perdre** notre communauté (si possible sans "fork").


---
## Comment ?

- En **développant** les logiciels v2;
- En **accompagnant les utilisateurs**, de manière pédagogique;
- En **formant** les futurs forgerons;
- En **étant transparent** sur les changements apportés par la V2;
- En **dialoguant** avec ceux qui ne veulent pas migrer, tout en respectant leur liberté;
- En **communiquant** auprès du grand public, à l'occasion de cet évènement;
- En **professionnalisant** nos productions (articles, vidéos), en agissant **collectivement**;

---
## Travail en commissions

Des **bénévoles de la communauté** Ğ1 se réunissent régulièrement.

Ils s'organisent en **petites commissions** autonomes,
**coordonnées** entre elles et **ouvertes à tous**.

---

Carte mentale des **acteurs impliqués** et actions à mener

![Schema](projects/coord/images/schema.svg)

---
## Liste des commissions

- [<i class="fa fa-home"></i> Accueil](projects/welcome/index.md) (`welcome`)
- [<i class="fa fa-calendar"></i> Coordination](projects/coord/index.md) (`coord`)
- [<i class="fa fa-bug"></i> Développement](projects/dev/index.md) (`dev`)
- [<i class="fa fa-bullhorn"></i> Communication](projects/com/index.md) (`com`)
- Accompagnement :
  - **<i class="fa fa-users"></i> Utilisateurs** (`users`)
  - **<i class="fa fa-cloud"></i> Forgerons** (`smiths`)
- [<i class="fa fa-flag"></i> Traduction](projects/lang/index.md) (`lang`)

note:
- Liste à compléter si besoin


---
## Visio du lundi

- Accueil des nouveaux (12h - 13h) : [meet.evolix.org/g1v2-welcome](https://meet.evolix.org/g1v2-welcome)<!-- .element target="_blank" --> |
- Début (en plénière) (13h) : [meet.evolix.org/coordinationV2](https://meet.evolix.org/coordinationV2)<!-- .element target="_blank" -->
- Commissions (13h15 - 14h) :
  |                                                           |                                                                                                       |
  |-----------------------------------------------------------|-------------------------------------------------------------------------------------------------------|
  | <i class="fa fa-calendar"></i> Coordination               | [meet.evolix.org/g1v2-coord](https://meet.evolix.org/g1v2-coord)<!-- .element target="_blank" -->     |
  | <i class="fa fa-bug"></i> Développement                   | [meet.evolix.org/g1v2-dev](https://meet.evolix.org/g1v2-dev)<!-- .element target="_blank" -->         |
  | <i class="fa fa-bullhorn"></i> Communication              | [meet.evolix.org/g1v2-com](https://meet.evolix.org/g1v2-com)<!-- .element target="_blank" -->         |
  | <i class="fa fa-users"></i> Accompagnement / Utilisateurs | [meet.evolix.org/g1v2-users](https://meet.evolix.org/g1v2-users)<!-- .element target="_blank" -->     |
  | <i class="fa fa-cloud"></i> Accompagnement / Forgerons    | [meet.evolix.org/g1v2-smiths](https://meet.evolix.org/g1v2-smiths)<!-- .element target="_blank" -->   |
  | <i class="fa fa-flag"></i> Traduction                     | [meet.evolix.org/g1v2-lang](https://meet.evolix.org/g1v2-lang)<!-- .element target="_blank" -->       |

- Retour en plénière (14h) 

---
## Compte-rendus de dernières réunions

- 2024 :
  - 27 mai : [compte-rendu](https://pad.p2p.legal/Visio_2024-05-27)
  - 13 mai : [compte-rendu](https://pad.p2p.legal/Visio_2024-05-13)
  - 29 avril : [compte-rendu](https://pad.p2p.legal/Visio_2024-04-29)
  - 22 avril : [compte-rendu](https://pad.p2p.legal/Visio_2024-04-22)
  - 25 mars : [compte-rendu](https://pad.p2p.legal/2024-03-25)
  - 15 avril : [compte-rendu](https://pad.p2p.legal/Visio_2024-04-15)
  - 8 avril : [compte-rendu](https://pad.p2p.legal/Visio_2024-04-08)
  - 18 mars : [compte-rendu](https://pad.p2p.legal/2024-03-18_cr-rml18) | [vidéo](https://tube.p2p.legal/w/64aosDsWMprqDptZkxfXxd)
  - 13 mars : [vidéo](https://tube.p2p.legal/w/dfKKYZL5gtWDSUiUQYQ6WB) (RML 18, à Laval)

Prochaine visio : **lundi 3 juin 2024** à 13h
