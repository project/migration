# Accueil

L'objectif est **d'accueillir** les nouveaux **bénévoles** qui veulent aider à la migration v2.

Elle les **renseigne** sur les différentes commissions.

> Le lundi, de **12h-13h**. 
> RDV sur : [meet.evolix.org/g1v2-welcome](https://meet.evolix.org/g1v2-welcome)<!-- .element target="_blank" -->

---

## Qui ?

Millicent, Maaltir, Annabelle
